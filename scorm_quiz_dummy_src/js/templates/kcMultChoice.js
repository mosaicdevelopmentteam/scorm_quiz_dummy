var aud;
var respAud;
var tl;

$(document).ready(function(){

	//hidePlaybackButtons();

	if(typeof slideHasBeenCompleted !== 'undefined'){
		if(slideHasBeenCompleted == true){
			//enter here what should happen if page has been completed
			console.log('page has been completed');

			//prevents Timeline from restarting when clicking pause then play
			slideEnded = false;

			showFeedback(getResponse());

			$('#btnNextFeedback').css('pointer-events', 'auto');
		}else{
			$('#btnNextFeedback').css('pointer-events', 'none');
			if(typeof playAudio === 'function'){
				playAudio(sound, tl, false); // (audio url, the TimelineMax, audio ended completes page)
			}else{
				var testAudio = new Audio(sound);
				testAudio.play();
			}
		}
	}

	$('.btn').on('click', function(){
		$this = $(this);

		if($this.hasClass('correct')){
			showFeedback("1");
		}else{
			showFeedback("0");
		}
	});

	$('#btnNextFeedback').on('click', function(){
		triggerNext();
	});

	function disableOptions(){
		$('.btn').off().css('pointer-events', 'none');
	}

	function showFeedback(s){
		disableOptions();

		setResponse(s);
		$('#btnNextFeedback').show();

		if(s == "0"){
			$('#feedback-incorrect').show();
			playResponseAudio('incorrect');
		}else if(s == "1"){
			$('#feedback-correct').show();
			playResponseAudio('correct');
		}

		$('.correct').css({'background-color':'rgba(19, 206, 19, 1)'});

		$('div#feedback').fadeIn();
	}

	function playResponseAudio(b){
		if(aud){
			aud.pause();
		}
		if(respAud){
			respAud.pause();
		}

		hidePlaybackButtons();

		if(b == 'correct'){
			responseUrl = correctSound;
		}else if(b == 'incorrect'){
			responseUrl = incorrectSound;
		}

		respAud = new Audio(responseUrl);
		respAud.addEventListener('loadeddata', function(){
		    respAud.play();
		}, false);
		respAud.addEventListener('ended', respAudioEnded);
	}

	function respAudioEnded(){
		enableNext();
		$('#btnNextFeedback').css('opacity', '1');
		$('#btnNextFeedback').css('pointer-events', 'auto');
		
		respAud.removeEventListener('ended', respAudioEnded);
	}

	initPage = function(){
		//define what should happen when intro audio has ended,
		//enabling user to interact with page elements if passing FALSE above

		$('#options div').css('pointer-events', 'auto');
	};

});