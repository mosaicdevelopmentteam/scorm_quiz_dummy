var clickedArray = [];
var clickedAll = false;
var clickedOne = false;
var aud;
var slideViewed = false;
var tl;

$(document).ready(function(){

	if(typeof slideHasBeenCompleted !== 'undefined'){
		if(slideHasBeenCompleted == true){
			//enter here what should happen if page has been completed
			console.log('page has been completed');

			//prevents Timeline from restarting when clicking pause then play
			slideEnded = false;

			clickedAll = true;
			clickedOne = true;

			slideViewed = true;

			$('#page section').css('opacity', '1');
			activateAllTabs();
		}
	}

	$('.image').each(function(){
		clickedArray.push(0);
	});

	$('.image').on('touchstart click', function(e){
		e.stopPropagation();

		$this = $(this);

		deActivateAllTabs();

		if(slideHasBeenCompleted == true){
			activateAllTabs();
		}

		//deactivate play/pause buttons when interacting with tabs
		updatePlayPause(true);
		if(typeof disablePlay === 'function'){
			disablePlay();//dr added to c6 - disable play button once tab audio is being played
		}

		if(aud){
			aud.pause();
		}

		var index = $('.image').index(this);
		var tl = new TimelineMax();
		tl.pause();

		switch(index){
			case 0:
				tl.to('.build:eq(0)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=4.0");
				tl.to('.build:eq(1)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=6.0");
				tl.to('.build:eq(2)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=0.5");
				tl.to('.build:eq(3)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=2.5");
				tl.to('.build:eq(4)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=4.0");
			break;

			case 1:
				tl.to('.build:eq(5)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=2.0");
				tl.to('.build:eq(6)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=3.0");
			break;

			case 2:
				tl.to('.build:eq(7)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=6.0");
				tl.to('.build:eq(8)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=4.5");
				tl.to('.build:eq(9)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=0.5");
				tl.to('.build:eq(10)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=1.5");
				tl.to('.build:eq(11)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=1.5");
			break;

			case 3:
				tl.to('.build:eq(12)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=7.0");
				tl.to('.build:eq(13)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=2.0");
				tl.to('.build:eq(14)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=3.0");
				tl.to('.build:eq(15)', 0.5, {opacity:1, ease:Linear.easeNone}, "+=2.5");
			break;
		}

		tl.play();

		//get tab audio file path
		var audioToPlay = $this.data('audio');

		var sectionToShow = $this.data('href');
		TweenMax.to($(sectionToShow), 0.5, {opacity:1, ease:Linear.easeNone});

		//create and play audio
		aud = new Audio(audioToPlay);
		aud.addEventListener('loadeddata', function(){
		    aud.play();
		}, false);
		aud.addEventListener('ended', tabAudioEnded);

		//if one tab has been clicked, change color of the rest of the tabs
		if(!clickedOne){
			$('.image').addClass('inactive');
			clickedOne = true;
		}

		//restore tab color
		//$this.removeClass('inactive');
		
		//the image index
		var index = $('.image').index(this);

		//clickedArray index set to 1
		clickedArray[index] = 1;

		//see if all tabs have been clicked
		checkNumViewed();
	});

	function tabAudioEnded(){
		if(clickedAll){
			setResponse('1');
			enableNext();
		}
		
		activateAllTabs();
	}

	function deActivateAllTabs(){
		$('.image').addClass('inactive');
	}

	function checkNumViewed(){
		var numViewed = 0;

		for(var i = 0; i < clickedArray.length; i++){
			if(clickedArray[i] == 1){
				numViewed++;
			}
		}

		if(!clickedAll){
			if(numViewed == clickedArray.length){
				clickedAll = true;
				// main.enableNext();
				// main.setResponse('1');
			}
		}
	}

	// true if audio ended marks slide complete,
	// false if slide interactivity marks slide complete
	if(typeof playAudio === 'function'){
		playAudio(sound, tl, false); // (audio url, the TimelineMax, audio ended completes page)
	}else{
		var testAudio = new Audio(sound);
		testAudio.play();
	}
});

activateAllTabs = function(){
	$('.image').removeClass('inactive');
};

initPage = function(){
	//define what should happen when intro audio has ended,
	//enabling user to interact with page elements if passing FALSE above

	activateAllTabs();
};