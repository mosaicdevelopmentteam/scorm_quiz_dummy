$(document).ready(function(){

	if(typeof slideHasBeenCompleted !== 'undefined'){
		if(slideHasBeenCompleted == true){
			//enter here what should happen if page has been completed
			console.log('page has been completed');

			//prevents Timeline from restarting when clicking pause then play
			slideEnded = false;

			$('#btnExit').css({'opacity':'1.0', 'pointer-events':'auto'});
		}
	}

	var tl = new TimelineMax();

	// true if audio ended marks slide complete,
	// false if slide interactivity marks slide complete
	if(typeof playAudio === 'function'){
		playAudio(sound, tl, false); // (audio url, the TimelineMax, audio ended completes page)
	}else{
		var testAudio = new Audio(sound);
		testAudio.play();
	}

	$('#btnExit').on('click', function(){
		exitCourse();
	});
});

initPage = function(){
	//define what should happen when intro audio has ended,
	//enabling user to interact with page elements if passing FALSE above

	$('#btnExit').css({'opacity':'1.0', 'pointer-events':'auto'});
};