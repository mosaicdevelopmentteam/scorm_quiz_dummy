var numAttempts = 0;
var numAttemptsAllowed = 2;
var correctString;
var selectedString;
var truncatedResponse = "";
var aud;
var tl;
var respAud;

$(document).ready(function(){

	//get the correct answers, create string of the indexes
	correctString = "";

	$('div#options div').each(function(i){
		$this = $(this);

		if($this.hasClass('correct')){
			correctString += i.toString();
		}
	});//end get correct answers

	if(typeof slideHasBeenCompleted !== 'undefined'){
		if(slideHasBeenCompleted == true){
			//enter here what should happen if page has been completed
			console.log('page has been completed');

			//prevents Timeline from restarting when clicking pause then play
			slideEnded = false;

			//deactivate play/pause buttons when interacting with tabs
			updatePlayPause(true);
			disablePlay();

			setTimeout(playResponseAudio, 500);


			var savedSelectionsArray = getResponse().split(',');
			console.log('split response array: ' + savedSelectionsArray);

			for(j = 0; j < savedSelectionsArray.length; j++){
				truncatedResponse += savedSelectionsArray[j].toString();
			}

			console.log('truncatedResponse: ' + truncatedResponse);

			$('div#options div').each(function(i){
				$this = $(this);

				if(isInArray(i.toString(), savedSelectionsArray)){
					$this.addClass('selected');
				}
			});

			checkAnswers(true);

			showFinalState();
		}else{
			if(typeof playAudio === 'function'){
				playAudio(sound, tl, false); // (audio url, the TimelineMax, audio ended completes page)
			}else{
				var testAudio = new Audio(sound);
				testAudio.play();
			}

			$('div#options div').on('click', function(i){
				$this = $(this);

				//highlight selection
				if($this.hasClass('selected')){
					$this.removeClass('selected');
				}else{
					$this.addClass('selected');
				}

				//show SUBMIT if at least one option is selected
				if($('.selected').length){
					$('#btnSubmit').show();
				}else{
					$('#btnSubmit').hide();
				}
			});
		}
	}

	function playResponseAudio(){
		// if(aud){
		// 	aud.pause();
		// }
		// if(respAud){
		// 	respAud.pause();
		// }
		// respAud = new Audio(responseUrl);
		// respAud.addEventListener('loadeddata', function(){
		//     respAud.play();
		// }, false);
		// respAud.addEventListener('ended', responseAudioEnded);
	}

	function responseAudioEnded(){
		enableNext();
		respAud.removeEventListener('ended', responseAudioEnded);
	}

	$('#btnSubmit').on('click', function(){
		$this = $(this);
		$this.hide();

		numAttempts++;

		selectedString = "";

		$('.selected').each(function(i){
			var index = $('div#options div').index(this);

			//setResponse(selectedString);

			if(i < $('.selected').length - 1){
				selectedString += index.toString() + ",";
			}else{
				selectedString += index.toString();
			}
		});

		truncatedResponse = selectedString.replace(/,/g, '');
		console.log('saving response of: ' + truncatedResponse);

		setResponse(selectedString);

		checkAnswers(false);
	});

	function checkAnswers(b){
		//b = if page has been completed before

		if(truncatedResponse == correctString){
			console.log('selected ' + truncatedResponse + ' is equal to ' + correctString);

			$('#feedback-correct').show();

			if(b){
				$('div#feedback').show();
			}else{
				$('div#feedback').fadeIn();
			}
			
			enableNext();
		}else{
			console.log('selected ' + truncatedResponse + ' is not equal to ' + correctString);

			if(b){
				$('div#options div').off().css('cursor', 'default');
				$('#feedback-incorrect2').show();
				$('div#feedback').show();

				enableNext();
			}else{
				if(numAttempts < numAttemptsAllowed){
					$('div#options div').css('pointer-events', 'none');
					$('div#options div').removeClass('selected');
					$('#feedback-incorrect1').show();

					$('div#feedback').fadeIn().delay(2000).fadeOut(function(){
						$('#feedback-incorrect1').hide();
						$('div#options div').css('pointer-events', 'auto');
					});
				}else{
					$('div#options div').off().css('cursor', 'default');
					$('#feedback-incorrect2').show();
					$('div#feedback').fadeIn();

					$('.correct').addClass('showcorrect');
					
					//comment out if there is response audio
					enableNext();

					//deactivate play/pause buttons when interacting with tabs
					updatePlayPause(true);
					disablePlay();
				}
			}
		}
	}

	function showFinalState(){
		enableNext();
		$('#btnSubmit').off().hide();
		$('div#options div').off().css('cursor', 'default');

		$('.correct').addClass('showcorrect');
	}
});

initPage = function(){
	//define what should happen when intro audio has ended,
	//enabling user to interact with page elements if passing FALSE above

	$('div#options').css('pointer-events', 'auto');
};

function isInArray(value, array) {
	return array.indexOf(value) > -1;
}




