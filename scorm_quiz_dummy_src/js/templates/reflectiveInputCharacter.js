var ableToEnterResponse = false;
var aud;
var tl;
var respAud;

$(document).ready(function() {

	if (typeof slideHasBeenCompleted !== 'undefined') {
		if (slideHasBeenCompleted == true) {
			//enter here what should happen if page has been completed
			console.log('page has been completed');

			//prevents Timeline from restarting when clicking pause then play
			slideEnded = false;

			//deactivate play/pause buttons when interacting with tabs
			updatePlayPause(true);
			disablePlay();

			$('#btnSubmit').remove();
			$('textarea').prop('disabled', true);

			$('textarea').val(getResponse()).show();

			$('#comparison-text').show();
			$('section#right').show();

			setTimeout(playResponseAudio, 500);
		}else{
			$('#character').css('opacity', 1);

			// true if audio ended marks slide complete,
			// false if slide interactivity marks slide complete
			if (typeof playAudio === 'function') {
				playAudio(sound, tl, false); // (audio url, the TimelineMax, audio ended completes page)
			} else {
				var testAudio = new Audio(sound);
				testAudio.play();
			}
		}
	}

	$("textarea").on("keyup", function() {
		if (ableToEnterResponse) {
			if ($(this).val().length) {
				$('#btnSubmit').show();
			} else {
				$('#btnSubmit').hide();
			}
		}
	});


	$('#btnSubmit').on('click', function() {
		$this = $(this);
		var studentEntry = $('textarea').val();
		setResponse(studentEntry);
		$this.hide();
		playResponseAudio();
		$('#character').hide();
		$('textarea').prop('disabled', true);
		$('#comparison-text').show();
		$('section#right').fadeIn();
		sessionStorage.setItem("complete", "true");
	});

	function playResponseAudio(){
		if(aud){
			aud.pause();
		}
		if(respAud){
			respAud.pause();
		}

		//deactivate play/pause buttons when interacting with tabs
		updatePlayPause(true);
		disablePlay();

		respAud = new Audio(responseUrl);
		respAud.addEventListener('loadeddata', function(){
		    respAud.play();
		}, false);
		respAud.addEventListener('ended', respAudioEnded);

		tl.play();
	}

	function respAudioEnded(){
		enableNext();
		respAud.removeEventListener('ended', respAudioEnded);
	}
});

initPage = function() {
	//define what should happen when intro audio has ended,
	//enabling user to interact with page elements if passing FALSE above

	ableToEnterResponse = true;
	if ($('textarea').val().length) {
		$('#btnSubmit').show();
	} else {
		$('#btnSubmit').hide();
	}
};