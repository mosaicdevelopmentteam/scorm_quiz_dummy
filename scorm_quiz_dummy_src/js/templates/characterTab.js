var clickedArray = [];
var clickedAll = false;
var clickedOne = false;
var aud;
var slideViewed = false;
var tl;

$(document).ready(function(){

	if(typeof slideHasBeenCompleted !== 'undefined'){
		if(slideHasBeenCompleted == true){
			//enter here what should happen if page has been completed
			console.log('page has been completed');

			//prevents Timeline from restarting when clicking pause then play
			slideEnded = false;

			clickedAll = true;
			clickedOne = true;

			slideViewed = true;

			activateAllTabs();
		}
	}

	$('#accordion h3').each(function(){
		clickedArray.push(0);
	});

	$('#accordion').accordion({
		collapsible: true,
		active: false,
		heightStyle: "content",
  		autoHeight: true
	});
	
	$('#accordion').delay(100).fadeIn();
	

	$('#accordion h3').on('touchstart click', function(e){
		e.stopPropagation();

		$this = $(this);

		if(typeof disablePlay === 'function'){
			disablePlay();//dr added to c6 - disable play button once tab audio is being played
		}
		

		deActivateAllTabs();

		if(slideHasBeenCompleted == true){
			activateAllTabs();
		}

		//deactivate play/pause buttons when interacting with tabs
		updatePlayPause(true);
		disablePlay();

		//parent.stopAnimateAudio();

		if(aud){
			aud.pause();
		}

		//get tab audio file path
		var audioToPlay = $this.data('audio');

		//create and play audio
		aud = new Audio(audioToPlay);
		aud.addEventListener('loadeddata', function(){
		    aud.play();
		}, false);
		aud.addEventListener('ended', tabAudioEnded);

		//if one tab has been clicked, change color of the rest of the tabs
		if(!clickedOne){
			$('#accordion h3').addClass('inactive');
			clickedOne = true;
		}

		//restore tab color
		$this.removeClass('inactive');
		
		//the accordion tab index
		var index = $('#accordion h3').index(this);

		//clickedArray index set to 1
		clickedArray[index] = 1;

		//see if all tabs have been clicked
		checkNumViewed();
	});

	function tabAudioEnded(){
		if(clickedAll){
			setResponse('1');
			enableNext();
		}
		
		activateAllTabs();
	}

	

	function deActivateAllTabs(){
		$('#accordion h3').addClass('inactive');
	}

	function checkNumViewed(){
		var numViewed = 0;

		for(var i = 0; i < clickedArray.length; i++){
			if(clickedArray[i] == 1){
				numViewed++;
			}
		}

		if(!clickedAll){
			if(numViewed == clickedArray.length){
				clickedAll = true;
				// main.enableNext();
				// main.setResponse('1');
			}
		}
	}

	if(typeof playAudio === 'function'){
		playAudio(sound, tl, false);
	}else{
		var testAudio = new Audio(sound);
		testAudio.play();
	}

});

activateAllTabs = function(){
	$('#accordion h3').removeClass('inactive');
};

initPage = function(){
	//define what should happen when intro audio has ended,
	//enabling user to interact with page elements if passing FALSE above
	activateAllTabs();
};