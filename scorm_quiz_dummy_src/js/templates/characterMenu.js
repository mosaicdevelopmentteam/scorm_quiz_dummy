var clickedArray = [];
var clickedAll = false;
var clickedOne = false;
var aud;
var tl;

$(document).ready(function(){

	if(typeof slideHasBeenCompleted !== 'undefined'){
		if(slideHasBeenCompleted == true){
			//enter here what should happen if page has been completed
			console.log('page has been completed');

			$('.char-button').addClass('viewed');

			//prevents Timeline from restarting when clicking pause then play
			slideEnded = false;

			activateAllTabs();
		}
	}

	// true if audio ended marks slide complete,
	// false if slide interactivity marks slide complete
	if(typeof playAudio === 'function'){
		playAudio(sound, tl, false); // (audio url, the TimelineMax, audio ended completes page)
	}else{
		var testAudio = new Audio(sound);
		testAudio.play();
	}

	$('.char-button').each(function(){
		$this = $(this);

		var btnTop = $this.data('top') + 'px';
		var btnLeft = $this.data('left') + 'px';

		$this.css({'top':btnTop, 'left':btnLeft});

		$this.prepend('<div class="square"></div>');

		//var bgImage = $this.data('bg');
		//$this.css('background-image', 'url(' + bgImage + ')');

		clickedArray.push(0);

	});

	$('.char-button').on('touchstart click', function(e){
		e.stopPropagation();

		$this = $(this);

		//deactivate play/pause buttons when interacting with tabs
		updatePlayPause(true);
		disablePlay();

		var charImg = $this.data('charimg');
		$('#characters').css('background-image', 'url(' + charImg + ')');

		if(typeof disablePlay === 'function'){
			disablePlay();//dr added to c6 - disable play button once tab audio is being played
		}
		
		// deActivateAllTabs();

		if(!$this.hasClass('viewed')){
			deActivateAllTabs();
		}else{
			$('#characters').fadeIn();
		}

		$this.addClass('viewed');

		$('#instruction').hide();

		if(aud){
			aud.pause();
		}

		//get tab audio file path
		var audioToPlay = $this.data('audio');

		//create and play audio
		aud = new Audio(audioToPlay);
		aud.addEventListener('loadeddata', function(){
		    aud.play();
		}, false);
		aud.addEventListener('ended', tabAudioEnded);

		//if one tab has been clicked, change color of the rest of the tabs
		if(!clickedOne){
			$('.char-button').addClass('inactive');
			clickedOne = true;
		}

		//restore tab color
		$this.removeClass('inactive');

		//the accordion tab index
		var index = $('.char-button').index(this);

		//clickedArray index set to 1
		clickedArray[index] = 1;

		//see if all tabs have been clicked
		checkNumViewed();
	});

	function tabAudioEnded(){
		if(clickedAll){
			setResponse('1');
			enableNext();
		}
		activateAllTabs();
	}

	

	function deActivateAllTabs(){
		if(slideHasBeenCompleted != true){
			$('.char-button').addClass('inactive');
		}
		
		$('.char-button').css({'cursor':'default', 'pointer-events':'none'});

		$('#characters').fadeIn();
	}

	function checkNumViewed(){
		var numViewed = 0;

		for(var i = 0; i < clickedArray.length; i++){
			if(clickedArray[i] == 1){
				numViewed++;
			}
		}

		if(!clickedAll){
			if(numViewed == clickedArray.length){
				clickedAll = true;
			}
		}
	}
});

activateAllTabs = function(){
	$('.char-button').removeClass('inactive');

	$('.char-button').css({'cursor':'pointer', 'pointer-events':'auto'});

	$('#characters').fadeOut();
};

initPage = function(){
	//define what should happen when intro audio has ended,
	//enabling user to interact with page elements if passing FALSE above
	activateAllTabs();
};
