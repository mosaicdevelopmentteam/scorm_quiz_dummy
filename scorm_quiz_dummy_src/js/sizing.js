/*Scale Vars*/
let $el = $("#scale_container");
let elHeight = $el.outerHeight();
let elWidth = $el.outerWidth();
let $wrapper = $("#main_container");

/************************Scale Code*****************************/
var scale, origin;

function doResize(event, ui) {
	scale = Math.min(
		ui.size.width / elWidth,
		ui.size.height / elHeight
	);

	//don't allow stage scale greater than 1
	if(scale > 1){
		scale = 1;
	}

	let topPosition = (ui.size.height - (elHeight * scale)) / 2;
	let leftPosition = (ui.size.width - (elWidth * scale)) / 2;

	$el.css({
		'transform': "translate(-0%, -0%) " + "scale(" + scale + ")",
		'top': topPosition,
		'left': leftPosition
	});

}

let starterData = {
	size: {
		width: window.innerWidth,
		height: window.innerHeight
	}
}

doResize(null, starterData);

window.addEventListener('resize', function(event) {
	let resizeData = {
		size: {
			width: window.innerWidth,
			height: window.innerHeight
		}
	}
	doResize(null, resizeData);
});