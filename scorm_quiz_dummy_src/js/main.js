studentResponseArray = [];
studentResponseString = "";

var testing;
var data = [];
var currentObjective = 0;
var maxObjectiveReached = 0;
var width = 1300;
var height = 740;
var scale;
var navExpanded = false;
var curPageNum = 0;
var maxPageNumReached = 0;//dr
var numPages;
var pagesLinear = [];
var objectiveLastSlideIndexes = [];
var objectiveLastSlideNum = 0;
var frames;
var tl;
var aud;
var slideEnded = false;
var unlockSlideAtEndOfAudio;
var respAud = new Audio();

// AUTOPLAY =====================
var userHasInteracted = false;
var initialPage;
// =====================

slideHasBeenCompleted = false;

$(document).ready(function() {

	frames = window.frames;

	$.getJSON("data/main.json", function(d) {
		//console.log("success");
		data = d;

		init();
	}).done(function() {
		//console.log('complete');
	}).fail(function(jqXHR, textStatus, errorThrown) {
		//console.log('fail');
	}).always(function() {
		//console.log('complete whether success or fail');
	});

	function getURLParams(){
	    var parameters = location.search.substring(1).split("&");

	    var temp = parameters[0].split("=");

		if(getParameterByName('testing') != '' && getParameterByName('testing') != undefined){
			testing = unescape(temp[1]);
		}else{
			testing = false;
		}

		if (testing) {
			document.onkeypress = function(e) {
				var charCodeValue = e.charCode;
				var keyTyped = String.fromCharCode(e.charCode);

				if (keyTyped == "z") {
					if(aud){
						aud.pause();
					}

					// AUTOPLAY =====================
					if($('#launch-modal').length){
						
						userHasInteracted = true;
						loadSlide(initialPage);
						$('#launch-modal').remove();
					}else{
						setResponse("1");
						$('#btnNext').trigger('click');
					}
					// setResponse("1");
					// $('#btnNext').trigger('click');
					// =====================
				}
			}
		}
	}

	function init() {
		getURLParams();
		
		if(testing){
			$('#scale_container').append('<div id="testadvance"></div>');
			$('#scale_container').append('<div id="status"></div>');

			//mimic an action that enables the NEXT button
			//after knowledge check, end of animation, etc.
			$('#testadvance').on('click', function() {
				$(this).hide();
				enableNext();
			});
		}

		//create left nav menu
		//create linear array of page urls
		for (var i = 0; i < data.objectives.length; i++) {
			$('nav').append('<div href="' + data.objectives[i].objectivePages[0] + '" class="objective-menuitem disabled" style="margin-top:' + (i * 83) + 'px;"><div class="chevron right"></div><span class="obj-num">' + (i + 1) + '</span><span class="obj-title">' + data.objectives[i].objectiveTitle + '</span></div>');

			//the last page number of each objective that when clicking NEXT will increment objective #
			objectiveLastSlideNum += data.objectives[i].objectivePages.length;
			objectiveLastSlideIndexes.push(objectiveLastSlideNum);

			//create empty student response for each page in course
			for (var s = 0; s < data.objectives[i].objectivePages.length; s++) {
				//studentResponseArray.push("a");

				//list page urls in a linear order
				pagesLinear.push(data.objectives[i].objectivePages[s]);
			}
		}

		//override fix to make first nav link NOT go to welcome screen
		$('.objective-menuitem').eq(0).attr('href', data.objectives[0].objectivePages[1]);

		$('span.obj-title').hover(function() {
			$(this).siblings('.chevron').show();
		}, function() {
			$(this).siblings('.chevron').hide();
		});


		//============================================
		// add downloads here if they exist in the JSON
		//============================================
		//alert(data.downloads.length);
		//alert(data.downloads[0].downloadTitle);
		//alert(data.downloads[0].downloadThumbnail);
		//alert(data.downloads[0].downloadURL);

		var numDownloads = data.downloads.length;

		if (numDownloads > 0) {
			$('nav').append('<div id="btnDownloads" class="objective-menuitem" style="margin-top:' + (data.objectives.length * 83) + 'px;"><span class="dl-icon"></span><span style="display:none;" class="dl-title">Downloads</span></div>');

			$('#scale_container').append('<aside id="downloads"><header>DOWNLOADS</header><div id="dl-close"></div></aside>');

			$('span.dl-title').on('click', function() {
				$('aside#downloads').show();
				TweenMax.to($('aside#downloads'), 0.3, {
					'right': '0',
					ease: Cubic.easeInOut
				});
			});

			$('#dl-close').on('click', function() {
				TweenMax.to($('aside#downloads'), 0.3, {
					'right': '-300px',
					ease: Cubic.easeInOut,
					onComplete: function() {
						$('aside#downloads').hide();
					}
				});
			});

			$('aside#downloads').append('<div id="dl-scroll"></div><div id="dl-gradient"></div>');//dr
			for (var i = 0; i < numDownloads; i++) {
				$('aside#downloads #dl-scroll').append('<div class="dl-link"><a href="' + data.downloads[i].downloadURL + '" target="_blank"><img src="' + data.downloads[i].downloadThumbnail + '"><p>' + data.downloads[i].downloadTitle + '</p></a></div>');
			}
		}

		// EV SCORM ADDITION
		let bookmark = ml_scorm.getBookmark()
		// AUTOPLAY =====================
		//let initialPage;
		// =====================

		if (bookmark) {
			console.log('found bookmark: ' + bookmark);
			console.log('resuming progress');
			let unpackedBookmark = bookmark.split('|');
			initialPage = unpackedBookmark[0];
			maxObjectiveReached = parseInt(unpackedBookmark[1]);
			currentObjective = maxObjectiveReached;
			console.log('maxObjectiveReached: ' + maxObjectiveReached);

			curPageNum = unpackedBookmark[2];

			maxPageNumReached = parseInt(unpackedBookmark[3]);

			//retrieve studentResponses
			if(ml_scorm.data){
				studentResponseArray = JSON.parse(ml_scorm.data);
			}

			for (let obj = 0; obj <= maxObjectiveReached; obj++) {
				console.log('disabling objective ' + obj);
				$('.objective-menuitem').eq(obj).removeClass('disabled');
			}
		} else {
			console.log("no bookmark present loading default");
			initialPage = pagesLinear[0];
			console.log('initial page is ' + initialPage);
		}

		//how many pages are there
		numPages = pagesLinear.length;


		// AUTOPLAY =====================
		//load first slide
		// loadSlide(initialPage);

		if(maxPageNumReached > 0){
			$('div#launch').css('background-image', 'url("images/neca_continuebutton_transp.jpg")');
		}else{
			$('div#launch').css('background-image', 'url("images/neca_startbutton_transp.jpg")');
		}
		// =====================

		//load objective on menu click (up to max objective reached)
		$('nav .objective-menuitem span.obj-title').on('click', function(e) {
			$this = $(this);

			$('iframe#objective-loader').hide();

			currentObjective = $('nav .objective-menuitem span.obj-title').index(this);


			//clicking objective menu button goes to first page of that objective
			var navIndex = $('nav .objective-menuitem span.obj-title').index(this);
			if (navIndex == 0) {
				curPageNum = 1;
			} else {
				curPageNum = 0;
				for (var o = 0; o < currentObjective; o++) {
					curPageNum = objectiveLastSlideIndexes[o];
				}
			}

			if (curPageNum == 0) {
				$('#btnPrev').hide();
			}

			var pageToLoad = $this.parent().attr('href');

			loadSlide(pageToLoad);
		});

		//make first objective menu item active
		$('.objective-menuitem').eq(currentObjective).removeClass('disabled');


		//convert array to string
		studentResponseString = studentResponseArray.join('|');
		console.log("studentResponseString: " + studentResponseString);

		//convert string back to array
		console.log("studentResponseArray: " + studentResponseString.split('|'));
	}

	$('#btnPlay').on('click', function() {
		$this = $(this);

		if(slideEnded){
			tl.restart();
			slideEnded = false;
		}

		updatePlayPause(false);

		aud.play();
		//TweenLite.to(tl, 0, {timeScale:1});

		tl.play();

		aud.addEventListener('ended', audioEnded);
		//aud.addEventListener("timeupdate", audioPlaying);
	});

	$('#btnPause').on('click', function() {
		$this = $(this);

		updatePlayPause(true);

		aud.pause();
		$this.addClass('play');
		//TweenLite.to(tl, 0, {timeScale:0});

		tl.pause();

		aud.removeEventListener('ended', audioEnded);
		//aud.removeEventListener("timeupdate", audioPlaying);
	});

	$('#btnPrev').on('click', function() {
		$this = $(this);

		if(aud){
			aud.pause();
		}
		if(respAud){
			respAud.pause();
		}

		if (curPageNum > 0) {
			curPageNum--;
			loadSlide(pagesLinear[curPageNum]);
		}

		if (curPageNum == 0) {
			$this.hide();
		}

		$('#btnNext').show();
	});

	$('#btnNext').on('click', function() {
		$('.questionRem').css('display', 'none')
		$this = $(this);

		if(aud){
			aud.pause();
		}
		if(respAud){
			respAud.pause();
		}

		//when on any slide before exit slide
		if (curPageNum < numPages - 1) {
			curPageNum++;
			loadSlide(pagesLinear[curPageNum]);
			showPlaybackButtons();
		}

		//when on exit slide
		if (curPageNum == numPages - 1) {
			console.log('show conclusion slide with exit button');
			//hidePlaybackButtons();
			$('#btnNext').hide();
		}

		$('#btnPrev').show();

		//update objective # when navigating linearly
		if (curPageNum == objectiveLastSlideIndexes[currentObjective]) {
			console.log('increment objective');
			++currentObjective;
			$('.objective-menuitem').eq(currentObjective).removeClass('disabled');
		}

		//set maxObjectiveReached
		if (maxObjectiveReached < currentObjective) {
			maxObjectiveReached = currentObjective;
			makeBookmark(pagesLinear[curPageNum]);
		}

		console.log('curPageNum: ' + curPageNum);
	});

	//toggle nav expand/collapse
	$('nav').on('click', function(e) {
		$this = $(this);

		if (navExpanded) {
			collapseNav();
		} else {
			expandNav();
		}
	});

	getResponse = function() {
		return studentResponseArray[curPageNum];
	};

	setResponse = function(s) {
		console.log('saving slide data: ' + s);

		studentResponseArray[curPageNum] = s;

		console.log(studentResponseArray);

		//========================================
		//SAVE DATA TO LMS HERE
		//========================================
		//dr - save studentResponses
		ml_scorm.data = JSON.stringify(studentResponseArray);
	};

	disablePlay = function() {
		$('#btnPlay').css({
			'opacity': '0.3',
			'pointer-events': 'none'
		});
	};

	enablePlay = function() {
		$('#btnPlay').css({
			'opacity': '1.0',
			'pointer-events': 'auto'
		});
	};

	updatePlayPause = function(b) {
		enablePlay();

		if (b) {
			$('#btnPlay').show();
			$('#btnPause').hide();
		} else {
			$('#btnPlay').hide();
			$('#btnPause').show();
		}
	}

	triggerNext = function() {
		$('#btnNext').trigger('click');
	};

	enableNext = function() {
		$('#btnNext').removeClass('disabled');
		$('#btnNext').addClass('pulse');
	};

	hidePlaybackButtons = function() {
		$('#btnNext, #btnPlay, #btnPause').hide();
	};

	hidePlayPause = function() {
		$('#btnPlay, #btnPause').hide();
		//$('#playPause').hide();
	};

	showPlaybackButtons = function() {
		$('#btnNext, #btnPlay, #btnPause').show();
	};

	exitCourse = function() {
		// EV SCORM ADDITIONS
		// mark SCO complete
		ml_scorm.completeSCO();
		ml_scorm.closeSCO();
		window.top.close();
	};

	if (testing) {
		document.onkeypress = function(e) {
			var charCodeValue = e.charCode;
			var keyTyped = String.fromCharCode(e.charCode);

			if (keyTyped == "z") {
				setResponse("temp saved response");
				$('#btnNext').trigger('click');
			}
		}
	}

	enableWelcome = function() {
		$('#btn-menu header').css({
			'cursor': 'pointer',
			'pointer-events': 'auto'
		}).on('click', function() {
			loadSlide(pagesLinear[0]);
			currentObjective = 0;
			curPageNum = 0;

			$('#btnPrev').hide();
			$('#btnNext').show();
			enableNext();
		});
	};

	function audioLoaded(){
		console.log("Playing " + aud.src + ", for: " + aud.duration + "seconds.");

		//totalTime = formatTime(Math.floor(aud.duration));

		// AUTOPLAY =====================
		if(userHasInteracted){
			aud.play();
		}
		// =====================
    	
    	aud.removeEventListener('loadedmetadata', audioLoaded);
	}

	function audioEnded(){
		console.log('audioEnded');

		slideEnded = true;

		updatePlayPause(true);

		if(unlockSlideAtEndOfAudio){
			setResponse('1');
			slideHasBeenCompleted = true;
			enableNext();
		}
		
		if(!unlockSlideAtEndOfAudio){
			initPage();
		}

		//if on welcome slide
		if(curPageNum == 0){
			enableWelcome();
		}
	}

	// function audioPlaying(){
	// 	curTime = formatTime(Math.floor(aud.currentTime));
		
	// 	$('#trt').text(curTime + " / " + totalTime);
	// }

	// function formatTime(nbSeconds, hasHours) {
	//     var time = [],
	//         s = 1;
	//     var calc = nbSeconds;

	//     if (hasHours) {
	//         s = 3600;
	//         calc = calc / s;
	//         time.push(format(Math.floor(calc)));//hour
	//     }

	//     calc = ((calc - (time[time.length-1] || 0)) * s) / 60;
	//     time.push(format(Math.floor(calc)));//minute

	//     calc = (calc - (time[time.length-1])) * 60;
	//     time.push(format(Math.round(calc)));//second


	//     function format(n) {//it makes "0X"/"00"/"XX"
	//         return (("" + n) / 10).toFixed(1).replace(".", "");
	//     }

	//     return time.join(":");
	// };

	playAudio = function(s, t, g){
		if(aud){
			aud.pause();
		}
		aud = new Audio(s);

		tl = t;

		//gated or not
		unlockSlideAtEndOfAudio = g;

		aud.addEventListener('loadedmetadata', audioLoaded);
		aud.addEventListener('ended', audioEnded);
		//aud.addEventListener("timeupdate", audioPlaying);
	};

	stopAllAudio = function(){
		if(aud){
			aud.pause();
		}
		if(respAud != 'undefined'){
			respAud.pause();
		}
	}

	// AUTOPLAY =====================
	$('div#launch').on('click', function(){
		userHasInteracted = true;
		$('#launch-modal').remove();
		//playSlide();
		
		loadSlide(initialPage);
	});
	// =====================
});


function expandNav() {
	navExpanded = true;

	$('div#btn-menu').addClass('no-bg');

	//animate nav right
	TweenMax.to($this, 0.2, {
		'width': '485px',
		ease: Cubic.easeInOut,
		onComplete: function() {
			//show close buttons
			$('div#btn-close, .dl-title, div#btn-menu header').fadeIn();

			//enable objective buttons
			$('nav .objective-menuitem').css('pointer-events', 'auto');
			$('nav .objective-menuitem span.obj-title').fadeIn();
		}
	});
}

function collapseNav() {
	navExpanded = false;

	//disable objective buttons
	$('nav .objective-menuitem').css('pointer-events', 'none');
	$('nav .objective-menuitem span.obj-title').hide();

	//hide close button
	$('div#btn-close, .dl-title, div#btn-menu header').hide();

	$('div#btn-menu').removeClass('no-bg');

	//animate nav left
	TweenMax.to($this, 0.2, {
		'width': '110px',
		ease: Cubic.easeInOut
	});
}

loadSlide = function(s){
	//console.log('loading slide: ' + s);

	stopAllAudio();

	$('#loader').load(s);

	updatePlayPause(false);

	if(studentResponseArray[curPageNum] != "" && studentResponseArray[curPageNum] != undefined){
		enableNext();

		slideHasBeenCompleted = true;

		if(testing){
			$('#testadvance').hide();
		}
	} else {
		$('#btnNext').addClass('disabled');

		slideHasBeenCompleted = false;

		if(testing){
			$('#testadvance').show();
		}
	}

	//enable next button if current page is before max page reached
	if(curPageNum < maxPageNumReached){
		enableNext();
	}

	//if bookmarked, set maxPageNumReached to index of last slide reached
	if(curPageNum > maxPageNumReached){
		maxPageNumReached = curPageNum;
	}

	//console.log('curPageNum: ' + curPageNum + " / numPages: " + numPages);
	if(curPageNum == numPages - 1){
		$('#btnNext').removeClass('disabled').hide();
	}else{
		$('#btnNext').css('display', 'block');
	}

	if(testing){
		var pageUrl = s;
		var res = pageUrl.slice(0, pageUrl.length);
		var newString = res.substr(0, res.length - 5);
		$('#status').html("Filename: <b>" + newString + "</b>");
	}

	if(curPageNum == 0){
		$('#btnPrev').hide();
	} else {
		$('#btnPrev').show();
	}

	//EV SCORM ADDITONS
	makeBookmark(s);
	//
};

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

