
SCORMinit();
window.addEventListener('unload', quit);
window.addEventListener('beforeunload', quit);


// call once to initialize all scorm variables. MUST be called before any SCORM
// functions will work.
function SCORMinit() {
  ml_scorm.initSCO();
  console.log('initing scorm');
}


// Call to handle all SCORM cleanup. Must be called to preserve any student data.
function quit() {
  ml_scorm.closeSCO();
}


// Called at the begining to reinstantiate the course to the state it was in
// when the student last left it.
function resumeProgress() {
  console.log('resuming progress');
  let bookmark = ml_scorm.getBookmark()

  if (bookmark)  {
    console.log('found bookmark: ' + bookmark);
    let unpackedBookmark = bookmark.split('|');
    loadSlide(unpackedBookmark[0]);
    for (let obj=0; obj < unpackedBookmark[1]; obj++)
    {
      $('.objective-menuitem').eq(obj).removeClass('disabled');
    }
  }
}

function makeBookmark(s) {
  let bookmark = s + '|' + maxObjectiveReached + '|' + curPageNum + '|' + maxPageNumReached;
  ml_scorm.setBookmark(bookmark);
}

